# == Class: radius::apt
#
# APT configuration for Freeradius systems
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2016-2017 The Board of Trustees of the Leland Stanford Junior University
#
class radius::apt (
  $sources
) {

  package {
    [
      'apt',
      'apt-transport-https',
    ]:
    ensure => latest,
  }

  class { 'apt':
    purge   => {
      'sources.list'   => false,
      'sources.list.d' => false,
    },
    sources => $sources,
  }

  # force apt-get update before package installation
  Class['apt::update'] -> Package<| |>

}

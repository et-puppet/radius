# Class: radius
# ===========================
#
# configure an Emerging Technology Freeradius server
#
# === Authors
#
# Scotty Logan <swl@stanford.edu>
#
# === Copyright
#
# Copyright (c) 2017 The Board of Trustees of the Leland Stanford Junior University
#
class radius (
  $docker,
  $docker_image
) {

  class { 'radius::packages': }

  if ($docker) {

    class { 'docker':
      version      => 'latest',
      docker_users => ['swl', 'vivienwu'],
    }

    docker::image { $docker_image: }

    docker::run { 'radiusd':
      image         => $docker_image,
      command       => '/usr/sbin/radiusd -X',
      ports         => ['1812/u', '1812', '1813/u', '1813' ],
      expose        => ['1812/u', '1812', '1813/u', '1813' ],
      volumes       => [
        '/dev/log',
        '/var/log/radius',
        '/etc/raddb'
      ],
      hostname      => 'radius-cert.stanford.edu',
      env           => [
        'KRB5_KTNAME=/etc/raddb/radius.keytab',
        'KRB5_CLIENT_KTNAME=/etc/raddb/radius.keytab'
      ],
      service       => 'always',
      privileged    => false,
      pull_on_start => true
    }

  } else {

  }

}

